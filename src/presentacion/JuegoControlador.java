package presentacion;

import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import logica.Fachada;
import utils.LocatedImage;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ResourceBundle;


public class JuegoControlador implements Initializable {

    private Fachada fachada;
    private String masa;
    private String salsa;
    private String topping;
    private String extra;

    private MediaPlayer sonido;
//    private Clip sonido;

    private Boolean entregable = false;

    @FXML
    private ImageView timerImg;

    @FXML
    private ImageView vidas;

    @FXML
    private RadioButton fina;

    @FXML
    private RadioButton gruesa;

    @FXML
    private RadioButton clasica;

    @FXML
    private RadioButton tomate;

    @FXML
    private RadioButton ranchera;

    @FXML
    private RadioButton barbacoa;

    @FXML
    private RadioButton emmental;

    @FXML
    private RadioButton gorgonzola;

    @FXML
    private RadioButton parmesano;

    @FXML
    private RadioButton olivas;

    @FXML
    private RadioButton champinyones;

    @FXML
    private RadioButton pepperoni;

    @FXML
    private ImageView screen;

    @FXML
    private Text dialogoMonster;

    @FXML
    private Label puntuacion;

    @FXML
    private Label precio;


    @FXML
    private Button hornoBtn;

    @FXML
    private Button entregaBtn;


    @Override
    public void initialize(URL location, ResourceBundle resources) {

        entregable = false;

        presentacion.IntroControlador.sonido.stop();
        inicializarSonido();

        //Inicializar Fachada
        setFachada();


        puntuacion.setText("Puntuaci�n: " + String.valueOf(fachada.getPuntuacion()));
        precio.setText("Precio: 0 �");
        ToggleGroup group = new ToggleGroup();
        fina.setToggleGroup(group);
        gruesa.setToggleGroup(group);
        clasica.setToggleGroup(group);

        ToggleGroup group2 = new ToggleGroup();
        tomate.setToggleGroup(group2);
        ranchera.setToggleGroup(group2);
        barbacoa.setToggleGroup(group2);

        ToggleGroup group3 = new ToggleGroup();
        emmental.setToggleGroup(group3);
        gorgonzola.setToggleGroup(group3);
        parmesano.setToggleGroup(group3);

        ToggleGroup group4 = new ToggleGroup();
        olivas.setToggleGroup(group4);
        champinyones.setToggleGroup(group4);
        pepperoni.setToggleGroup(group4);


        ((ImageView) clasica.getGraphic()).setImage(new LocatedImage("images/ingredientes/clasica.png"));
        ((ImageView) fina.getGraphic()).setImage(new LocatedImage("images/ingredientes/fina.png"));
        ((ImageView) gruesa.getGraphic()).setImage(new LocatedImage("images/ingredientes/gruesa.png"));

        ((ImageView) tomate.getGraphic()).setImage(new LocatedImage("images/ingredientes/tomate.png"));
        ((ImageView) ranchera.getGraphic()).setImage(new LocatedImage("images/ingredientes/ranchera.png"));
        ((ImageView) barbacoa.getGraphic()).setImage(new LocatedImage("images/ingredientes/barbacoa.png"));

        ((ImageView) emmental.getGraphic()).setImage(new LocatedImage("images/ingredientes/emmental.png"));
        ((ImageView) gorgonzola.getGraphic()).setImage(new LocatedImage("images/ingredientes/gouda.png"));
        ((ImageView) parmesano.getGraphic()).setImage(new LocatedImage("images/ingredientes/parmesano.png"));

        ((ImageView) olivas.getGraphic()).setImage(new LocatedImage("images/ingredientes/olivas.png"));
        ((ImageView) champinyones.getGraphic()).setImage(new LocatedImage("images/ingredientes/champinyon.png"));
        ((ImageView) pepperoni.getGraphic()).setImage(new LocatedImage("images/ingredientes/pepperoni.png"));


        group.selectedToggleProperty().addListener((observ, oldValue, newValue) -> {

            if (group.getSelectedToggle().equals(fina)) {
                ((ImageView) clasica.getGraphic()).setImage(new LocatedImage("images/ingredientes/clasica.png"));
                ((ImageView) fina.getGraphic()).setImage(new LocatedImage("images/ingredientes/finaOK.png"));
                ((ImageView) gruesa.getGraphic()).setImage(new LocatedImage("images/ingredientes/gruesa.png"));
                masa = "fina";
            }
            if (group.getSelectedToggle().equals(gruesa)) {
                ((ImageView) clasica.getGraphic()).setImage(new LocatedImage("images/ingredientes/clasica.png"));
                ((ImageView) fina.getGraphic()).setImage(new LocatedImage("images/ingredientes/fina.png"));
                ((ImageView) gruesa.getGraphic()).setImage(new LocatedImage("images/ingredientes/gruesaOK.png"));
                masa = "gruesa";
            }
            if (group.getSelectedToggle().equals(clasica)) {
                ((ImageView) clasica.getGraphic()).setImage(new LocatedImage("images/ingredientes/clasicaOK.png"));
                ((ImageView) fina.getGraphic()).setImage(new LocatedImage("images/ingredientes/fina.png"));
                ((ImageView) gruesa.getGraphic()).setImage(new LocatedImage("images/ingredientes/gruesa.png"));
                masa = "clasica";
            }

        });


        group2.selectedToggleProperty().addListener((observ, oldValue, newValue) -> {

            if (group2.getSelectedToggle().equals(tomate)) {
                ((ImageView) tomate.getGraphic()).setImage(new LocatedImage("images/ingredientes/tomateOK.png"));
                ((ImageView) ranchera.getGraphic()).setImage(new LocatedImage("images/ingredientes/ranchera.png"));
                ((ImageView) barbacoa.getGraphic()).setImage(new LocatedImage("images/ingredientes/barbacoa.png"));
                salsa = "tomate";
            }
            if (group2.getSelectedToggle().equals(ranchera)) {
                ((ImageView) tomate.getGraphic()).setImage(new LocatedImage("images/ingredientes/tomate.png"));
                ((ImageView) ranchera.getGraphic()).setImage(new LocatedImage("images/ingredientes/rancheraOK.png"));
                ((ImageView) barbacoa.getGraphic()).setImage(new LocatedImage("images/ingredientes/barbacoa.png"));
                salsa = "ranchera";
            }
            if (group2.getSelectedToggle().equals(barbacoa)) {
                ((ImageView) tomate.getGraphic()).setImage(new LocatedImage("images/ingredientes/tomate.png"));
                ((ImageView) ranchera.getGraphic()).setImage(new LocatedImage("images/ingredientes/ranchera.png"));
                ((ImageView) barbacoa.getGraphic()).setImage(new LocatedImage("images/ingredientes/barbacoaOK.png"));
                salsa = "barbacoa";
            }
        });

        group3.selectedToggleProperty().addListener((observ, oldValue, newValue) -> {

            if (group3.getSelectedToggle().equals(emmental)) {
                ((ImageView) emmental.getGraphic()).setImage(new LocatedImage("images/ingredientes/emmentalOK.png"));
                ((ImageView) gorgonzola.getGraphic()).setImage(new LocatedImage("images/ingredientes/gouda.png"));
                ((ImageView) parmesano.getGraphic()).setImage(new LocatedImage("images/ingredientes/parmesano.png"));
                topping = "emmental";
            }
            if (group3.getSelectedToggle().equals(gorgonzola)) {
                ((ImageView) emmental.getGraphic()).setImage(new LocatedImage("images/ingredientes/emmental.png"));
                ((ImageView) gorgonzola.getGraphic()).setImage(new LocatedImage("images/ingredientes/goudaOK.png"));
                ((ImageView) parmesano.getGraphic()).setImage(new LocatedImage("images/ingredientes/parmesano.png"));
                topping = "gorgonzola";
            }
            if (group3.getSelectedToggle().equals(parmesano)) {
                ((ImageView) emmental.getGraphic()).setImage(new LocatedImage("images/ingredientes/emmental.png"));
                ((ImageView) gorgonzola.getGraphic()).setImage(new LocatedImage("images/ingredientes/gouda.png"));
                ((ImageView) parmesano.getGraphic()).setImage(new LocatedImage("images/ingredientes/parmesanoOK.png"));
                topping = "parmesano";
            }
        });

        group4.selectedToggleProperty().addListener((observ, oldValue, newValue) -> {

            if (group4.getSelectedToggle().equals(olivas)) {
                ((ImageView) olivas.getGraphic()).setImage(new LocatedImage("images/ingredientes/olivasOK.png"));
                ((ImageView) champinyones.getGraphic()).setImage(new LocatedImage("images/ingredientes/champinyon.png"));
                ((ImageView) pepperoni.getGraphic()).setImage(new LocatedImage("images/ingredientes/pepperoni.png"));
                extra = "olivas";
            }
            if (group4.getSelectedToggle().equals(champinyones)) {
                ((ImageView) olivas.getGraphic()).setImage(new LocatedImage("images/ingredientes/olivas.png"));
                ((ImageView) champinyones.getGraphic()).setImage(new LocatedImage("images/ingredientes/champinyonOK.png"));
                ((ImageView) pepperoni.getGraphic()).setImage(new LocatedImage("images/ingredientes/pepperoni.png"));
                extra = "champis";
            }
            if (group4.getSelectedToggle().equals(pepperoni)) {
                ((ImageView) olivas.getGraphic()).setImage(new LocatedImage("images/ingredientes/olivas.png"));
                ((ImageView) champinyones.getGraphic()).setImage(new LocatedImage("images/ingredientes/champinyon.png"));
                ((ImageView) pepperoni.getGraphic()).setImage(new LocatedImage("images/ingredientes/pepperoniOK.png"));
                extra = "pepperoni";
            }
        });



    }


    public void cambiarPizzaContador(int indiceImagen) {
        timerImg.setImage(new LocatedImage("images/contadortiempo/" + indiceImagen + ".png"));
    }


    //TODO sacar clase LocatedImage para usarla en varios sitios a la vez, meterla en "utils"


    @FXML
    public void entregaPizza() {
        if (entregable) {
            String respuesta = fachada.entregaPizza();
            if (respuesta.equals("pierdePartida")) {
                dialogoMonster.setText("Como cocinero no eres muy bueno... quiz� como comida seas mejor~~");
                //Pausa por 2 segundos
                //TODO pausa gameover
                gameOver();
            } else {
                dialogoMonster.setText(respuesta);
            }

            puntuacion.setText("Puntuaci�n: " + String.valueOf(fachada.getPuntuacion()));
            cambiarVidas(fachada.getVidasPlayer());
            if (fachada.compararPizzas() == 0) {
                reproduceSonido("eating.wav");
                screen.setImage(new LocatedImage("images/monsterhappy.png"));
                fachada.reiniciarPizzaMonster();
            } else {
                reproduceSonido("angry.wav");
                screen.setImage(new LocatedImage("images/monsterangry.png"));
            }
        } else {
            dialogoMonster.setText("No quiero una pizza sin cocinar...");
        }

    }

    public void cambiarVidas(int vidasPlayer) {
        System.out.println("images/vidas/" + vidasPlayer + "lifes.png");

        this.vidas.setImage(new LocatedImage("images/vidas/" + vidasPlayer + "lifes.png"));
       // if (vidasPlayer==0) gameOver();
    }

    @FXML
    public void cocinarPizza() {
        try {
            if (!fachada.esPizzaSinIngredientes()) {
                fachada.reiniciarPizza();
            }
            screen.setImage(new LocatedImage("images/monster.png"));
            int rand = (int) Math.floor(Math.random() * 4 + 1);
            switch (rand) {
                case 1:
                    dialogoMonster.setText("�Tengo m�s hambre que el perro de un ciego!");
                    break;
                case 2:
                    dialogoMonster.setText("�Tengo m�s hambre que el Tamagotchi de un sordo!");
                    break;
                case 3:
                    dialogoMonster.setText("�Tengo un hambre que me da calambre!");
                    break;
                default:
                    dialogoMonster.setText("Ya est� hecha, �no? �Trae para ac�!");
            }
            fachada.addIngredientes(masa);
            fachada.addIngredientes(salsa);
            fachada.addIngredientes(topping);
            fachada.addIngredientes(extra);
            reproduceSonido("bell.wav");
            precio.setText("Precio: " + String.valueOf(fachada.getPrecio()) + " �");
            entregable = true;
        } catch (Exception e) {
            dialogoMonster.setText("��En esa pizza faltan ingredientes!!");
            screen.setImage(new LocatedImage("images/monsterangry.png"));
        }

    }

    public void inicializarSonido() {
        Media m = null;
        try {
            m = new Media(IntroControlador.class.getResource("/sonidos/pizzagame.mp3").toURI().toString());
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        sonido = new MediaPlayer(m);

        sonido.play();
        System.out.print(sonido.getCurrentRate());
//        sonido.setRate(0.06);

//        try {
//            sonido = AudioSystem.getClip();
//
//            String filePath = new File("").getAbsolutePath();
//            File archivo = new File(filePath + "/src/sonidos/pizzagame.wav");
//
//            sonido.open(AudioSystem.getAudioInputStream(archivo));
//            //bucle infinito en musica
//            sonido.loop(Clip.LOOP_CONTINUOUSLY);
//            sonido.start();
//        } catch (Exception e) {
//            System.out.print(e.getMessage());
//        }

    }

    public void reproduceSonido(String son) {
        Media m = null;
        try {
            m = new Media(IntroControlador.class.getResource("/sonidos/"+son).toURI().toString());
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        MediaPlayer player = new MediaPlayer(m);
        player.play();
//        try {
//            Clip sfx = AudioSystem.getClip();
//            String filePath = new File("").getAbsolutePath();
//            File archivo = new File(filePath + "/src/sonidos/" + son);
//            sfx.open(AudioSystem.getAudioInputStream(archivo));
//            sfx.start();
//        } catch (Exception e) {
//            System.out.print(e.getMessage());
//        }
    }

    public void gameOver() {

        fachada.reiniciarJuego();
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("Intro.fxml"));

        Stage stage = (Stage) barbacoa.getScene().getWindow();





        //Intencionadamente llamamos a quitar todas las vidas

            cambiarVidas(0);
            screen.setImage(new LocatedImage("images/monsterangry.png"));



        ////////////////
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("GAME OVER");
        alert.setHeaderText(null);
        ImageView imagenView = new ImageView(new Image("/presentacion/pizzamaniaicon.png"));
        imagenView.setFitHeight(100);
        imagenView.setFitWidth(100);
        alert.getDialogPane().setStyle("-fx-font-size: 15px; -fx-alignment: center");
        alert.setGraphic(imagenView);
        alert.initOwner(stage);
        alert.setContentText("\n���Aprende a cocinar!!! " + puntuacion.getText() + (puntuacion.getText().equals("1.0") ? " punto." : " puntos."));



        alert.setOnCloseRequest(new EventHandler<DialogEvent>() {
            @Override
            public void handle(DialogEvent event) {
                sonido.stop();
            }
        });
        alert.showAndWait();
        ////////////////

        //Inicializamos pantalla incial aqui para que no empiece ya el sonido de la otra sin pulsar ok
        Parent raiz = null;
        try {
            raiz = (Parent) loader.load();
        } catch (IOException e) {
            System.out.print(e);
        }

        Scene scene = new Scene(raiz);
        scene.getStylesheets().add("images/style.css");


        IntroControlador controlador = (IntroControlador) loader.getController();

        stage.setScene(scene);

    }

    public void setFachada() {
        fachada = Fachada.getFachada();
    }


    public void gameOverEspecial() {

        gameOver();
    }
}