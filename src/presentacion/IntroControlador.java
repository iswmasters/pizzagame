package presentacion;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.stage.Stage;
import logica.Fachada;
import utils.LocatedImage;

import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ResourceBundle;

public class IntroControlador extends Application implements Initializable {

    public static MediaPlayer sonido;
    private Fachada fachada;
    private Parent vistaRaiz;
    private Scene escenaPrincipal;
    private Stage primaryStage;

    @FXML
    private Button facil;
    @FXML
    private Button medio;
    @FXML
    private Button dificil;

    final EventHandler<ActionEvent> myHandler = new EventHandler<ActionEvent>() {
        @Override
        public void handle(final ActionEvent event) {
            Button x = (Button) event.getSource();
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("Juego.fxml"));

            Stage stage = (Stage) facil.getScene().getWindow();

            Parent raiz = null;
            try {
                raiz = (Parent) loader.load();
            } catch (IOException e) {
                System.out.print(e);
            }

            Scene scene = new Scene(raiz);
            scene.getStylesheets().add("images/style.css");

            JuegoControlador controlador = loader.getController();


            stage.setScene(scene);

            fachada = Fachada.getFachada();

            fachada.setControladorJuego(controlador);

            fachada.elegirDificultad(Integer.parseInt(x.getId()));

            //Iniciamos contador de tiempo (una vez hemos selecciionado nivel para que tengamos asignada su referencia en memoria dentro de this.nivel para el metodo contadorTiempo())
            fachada.contadorTiempo();

            //Iniciar Pizza
            fachada.seleccionarPizza();


        }

    };


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        this.inicializarSonido();

        facil.setId("1");
        facil.setOnAction(myHandler);

        medio.setId("2");
        medio.setOnAction(myHandler);

        dificil.setId("3");
        dificil.setOnAction(myHandler);


        facil.addEventHandler(MouseEvent.MOUSE_ENTERED, event -> ((ImageView) facil.getGraphic()).setImage(new LocatedImage("images/facilpush.png")) );
        facil.addEventHandler(MouseEvent.MOUSE_EXITED, event -> ((ImageView) facil.getGraphic()).setImage(new LocatedImage("images/facil.png")) );

        medio.addEventHandler(MouseEvent.MOUSE_ENTERED, event -> ((ImageView) medio.getGraphic()).setImage(new LocatedImage("images/normalpush.png")) );
        medio.addEventHandler(MouseEvent.MOUSE_EXITED, event -> ((ImageView) medio.getGraphic()).setImage(new LocatedImage("images/normal.png")) );

        dificil.addEventHandler(MouseEvent.MOUSE_ENTERED, event -> ((ImageView) dificil.getGraphic()).setImage(new LocatedImage("images/dificilpush.png")) );
        dificil.addEventHandler(MouseEvent.MOUSE_EXITED, event -> ((ImageView) dificil.getGraphic()).setImage(new LocatedImage("images/dificil.png")) );

    }

    @Override
    public void start(Stage primaryStage) throws IOException {

        this.primaryStage = primaryStage;
        primaryStage.setResizable(false);

        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("Intro.fxml"));
        vistaRaiz = (Parent) loader.load();

        escenaPrincipal = new Scene(vistaRaiz);
        primaryStage.setScene(escenaPrincipal);


        primaryStage.getIcons().add(new Image(getClass().getResourceAsStream("pizzamaniaicon.png")));



        primaryStage.show();
        //inicializarSonido();

    }

    //Iniciar sonido
    public void inicializarSonido() {


        Media m = null;
        try {
            m = new Media(IntroControlador.class.getResource("/sonidos/happy.mp3").toURI().toString());
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        sonido = new MediaPlayer(m);
        sonido.play();
//        try {
//            sonido = AudioSystem.getClip();
//
//            String filePath = new File("").getAbsolutePath();
//            File archivo = new File(filePath + "/src/sonidos/happy.wav");
//
//
//            sonido.open(AudioSystem.getAudioInputStream(archivo));
//
//            //bucle infinito en musica
//            sonido.loop(Clip.LOOP_CONTINUOUSLY);
//            sonido.start();
//
//        } catch (Exception e) {
//            System.out.print(e.getMessage());
//        }

    }

    public static void main(String[] args) {
        launch(args);
    }

}
