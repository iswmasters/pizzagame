package utils;

import javafx.scene.image.Image;

/**
 * Created by David on 29/05/2016.
 */
public class LocatedImage extends Image {
    private final String url;

    public LocatedImage(String url) {
        super(url);
        this.url = url;
    }


}