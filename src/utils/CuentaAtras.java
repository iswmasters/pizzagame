package utils;

import logica.Fachada;
import presentacion.JuegoControlador;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by David on 25/05/2016.
 */
public class CuentaAtras {
    Contador count;
    int segundosIniciales;
    private JuegoControlador controladorJuego;
    boolean entrado=false;

    public CuentaAtras(int segundosIniciales) {
        this.segundosIniciales = segundosIniciales;
        count = new Contador(segundosIniciales);


        //Intervalo entre imagenes
        int intervalo = segundosIniciales / 6;

        TimerTask time = new TimerTask() {
            public void run() {

                int segundosRestantes = count.reducir();
                System.out.println(segundosRestantes);


                //Cada instante de tiempo cambia la imagen
                if (segundosRestantes % intervalo == 0) {
                    int indiceImagen = segundosRestantes / intervalo;
                    System.out.println("Instante cambiar imagen a: " + indiceImagen);
                    controladorJuego.cambiarPizzaContador(indiceImagen);
                }


                if (((double)segundosRestantes / (double)segundosIniciales) < 0.4 && !entrado){
                    System.out.print("40% tiempo restante");
                    entrado=true;


                }


                if (segundosRestantes <= 0) {
                    Fachada.getFachada().decrementaVida();
                    int vidas = Fachada.getFachada().getVidasPlayer();
                    controladorJuego.cambiarVidas(vidas);
                    reset();
                }

            }
        };
        Timer timer = new Timer();
        timer.scheduleAtFixedRate(time, 0, 1000);


    }


    public void reset() {
        //Inicializamos la imagen de la pizza a 6
        controladorJuego.cambiarPizzaContador(6);
        count.reset(segundosIniciales);

    }

    public void setControladorJuego(JuegoControlador controladorJuego) {
        this.controladorJuego = controladorJuego;
    }



    /**
     * Clase interna Contador para extender la funcionalidad del Timer original de Java
     */
    class Contador {
        int count;
        boolean stillTime;

        public Contador(int count) {
            this.count = count;
            stillTime = true;
        }

        int reducir() {
            if (count > 0) count--;
            else stillTime = false;
            return count;
        }

        boolean reset(int c) {
            if (stillTime) count = c;
            return stillTime;
        }
    }


}