package logica.fabrica;

import logica.estrategia.Nivel;
import logica.estrategia.NivelDificil;
import logica.estrategia.NivelFacil;
import logica.estrategia.NivelMedio;

public class FabricaNiveles {
    private static FabricaNiveles fabricaNiveles = null;


    private FabricaNiveles() {
        //Privado !!!
    }

    public static FabricaNiveles getFabricaNiveles() {
        if (fabricaNiveles == null) fabricaNiveles = new FabricaNiveles();
        return fabricaNiveles;
    }



    public Nivel NivelFacil() {
        Nivel nivel = new NivelFacil();
        return nivel;
    }

    public Nivel NivelMedio() {
        Nivel nivel = new NivelMedio();
        return nivel;
    }

    public Nivel NivelDificil() {
        Nivel nivel = new NivelDificil();
        return nivel;
    }


    public Nivel elegirNivel(int nivel) {
        Nivel res = null;
        switch (nivel) {
            case 1:
                res = NivelFacil();
                break;
            case 2:
                res = NivelMedio();
                break;
            case 3:
                res = NivelDificil();
                break;
        }
        return res;
    }
}

