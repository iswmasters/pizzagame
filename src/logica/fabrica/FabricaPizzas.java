package logica.fabrica;

import logica.*;
import logica.decorador.*;


public class FabricaPizzas {

    public FabricaPizzas() {

    }

    public static PizzaInterface crearPeque() {
        PizzaInterface pizza = new PizzaPeque();
        return pizza;
    }

    public static PizzaInterface crearMedia() {
        PizzaInterface pizza = new PizzaMedia();
        return pizza;
    }

    public static PizzaInterface crearGrande() {
        PizzaInterface pizza = new PizzaGrande();
        return pizza;
    }

    public static PizzaInterface crearPizzaRand(PizzaInterface pizzaR) {
        PizzaInterface pizza = pizzaR;
        int rand = (int) Math.floor(Math.random() * 3 + 1);
        switch (rand) {
            case 1:
                pizza = new MasaClasicaDecorator(pizza);
                break;
            case 2:
                pizza = new MasaFinaDecorator(pizza);
                break;
            case 3:
                pizza = new MasaGruesaDecorator(pizza);
                break;
        }
        rand = (int) Math.floor(Math.random() * 3 + 1);
        switch (rand) {
            case 1:
                pizza = new SalsaTomateDecorator(pizza);
                break;
            case 2:
                pizza = new SalsaRancheraDecorator(pizza);
                break;
            case 3:
                pizza = new SalsaBarbacoaDecorator(pizza);
                break;
        }
        rand = (int) Math.floor(Math.random() * 3 + 1);
        switch (rand) {
            case 1:
                pizza = new ToppingEmmentalDecorator(pizza);
                break;
            case 2:
                pizza = new ToppingGorgonzollaDecorator(pizza);
                break;
            case 3:
                pizza = new ToppingParmesanoDecorator(pizza);
                break;
        }
        rand = (int) Math.floor(Math.random() * 3 + 1);
        switch (rand) {
            case 1:
                pizza = new ExtraOlivasDecorator(pizza);
                break;
            case 2:
                pizza = new ExtraChampisDecorator(pizza);
                break;
            case 3:
                pizza = new ExtraPepperoniDecorator(pizza);
                break;
        }
        return pizza;
    }


    public static Monster crearMonster(PizzaInterface pizza) {
        return new Monster(pizza);
    }

    public static PizzaInterface addOlivas(PizzaInterface pizza) {
        return new ExtraOlivasDecorator(pizza);
    }

    public static PizzaInterface addChampis(PizzaInterface pizza) {
        return new ExtraChampisDecorator(pizza);
    }

    public static PizzaInterface addPepperoni(PizzaInterface pizza) {
        return new ExtraPepperoniDecorator(pizza);
    }

    public static PizzaInterface addClasica(PizzaInterface pizza) {
        return new MasaClasicaDecorator(pizza);
    }

    public static PizzaInterface addFina(PizzaInterface pizza) {
        return new MasaFinaDecorator(pizza);
    }

    public static PizzaInterface addGruesa(PizzaInterface pizza) {
        return new MasaGruesaDecorator(pizza);
    }

    public static PizzaInterface addBarbacoa(PizzaInterface pizza) {
        return new SalsaBarbacoaDecorator(pizza);
    }

    public static PizzaInterface addRanchera(PizzaInterface pizza) {
        return new SalsaRancheraDecorator(pizza);
    }

    public static PizzaInterface addTomate(PizzaInterface pizza) {
        return new SalsaTomateDecorator(pizza);
    }

    public static PizzaInterface addEmmental(PizzaInterface pizza) {
        return new ToppingEmmentalDecorator(pizza);
    }

    public static PizzaInterface addGorgonzolla(PizzaInterface pizza) {
        return new ToppingGorgonzollaDecorator(pizza);
    }

    public static PizzaInterface addParmesano(PizzaInterface pizza) {
        return new ToppingParmesanoDecorator(pizza);
    }
}
