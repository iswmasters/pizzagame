package logica;

public class PizzaMedia implements PizzaInterface {
    private PizzaInterface anterior;
    private String descripcion;
    private double precio;

    public PizzaMedia() {
        this.anterior = null;
        this.descripcion = "Pizza mediana";
        this.precio = 10.0;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public double calculaCoste() {
        return precio;
    }

    public PizzaInterface getAnterior() {
        return anterior;
    }

    public void setAnterior(PizzaInterface anterior) {
        this.anterior = anterior;
    }
}

