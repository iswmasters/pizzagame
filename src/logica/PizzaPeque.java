package logica;

public class PizzaPeque implements PizzaInterface {
    private PizzaInterface anterior;
    private String descripcion;
    private double precio;

    public PizzaPeque() {
        this.anterior = null;
        this.descripcion = "Pizza peque�a";
        this.precio = 5.0;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public double calculaCoste() {
        return precio;
    }

    public PizzaInterface getAnterior() {
        return anterior;
    }

    public void setAnterior(PizzaInterface anterior) {
        this.anterior = anterior;
    }
}
