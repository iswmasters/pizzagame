package logica;


public class Monster {
    private String[] respuestas = new String[5];
    private PizzaInterface pizza;

    public Monster(PizzaInterface pizza) {
        this.pizza = pizza;
        respuestas[0] = "�am, �am... muy rica~~";
        respuestas[1] = "��No tiene mi EXTRA favorito??";
        respuestas[2] = "��Este QUESO es repugnante!!";
        respuestas[3] = "��D�nde est� la SALSA que quiero?!";
        respuestas[4] = "�Esta MASA no me gusta!";
    }

    public PizzaInterface getPizza() {
        return this.pizza;
    }

    public String[] getRespuestas() {
        return this.respuestas;
    }
}
