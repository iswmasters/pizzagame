package logica;

import logica.estrategia.Nivel;
import logica.fabrica.FabricaNiveles;
import logica.fabrica.FabricaPizzas;
import presentacion.JuegoControlador;

public class Fachada {
    private static Fachada fachada = null;
    private MainLogica main;
    private JuegoControlador controladorJuego;
    private Nivel nivel;


    /**
     * Constructor de la clase Fachada
     */
    protected Fachada() {
        this.setMain(new MainLogica());
    }

    public static Fachada getFachada() {
        if (fachada == null) fachada = new Fachada();
        return fachada;
    }


    public void elegirDificultad(int dificultad) {
        Nivel nivelSeleccionado = FabricaNiveles.getFabricaNiveles().elegirNivel(dificultad);
        this.setNivel(nivelSeleccionado);

    }

    public void addIngredientes(String ingrediente) {
        switch (ingrediente) {
            case "fina":
                this.getMain().setPizza(FabricaPizzas.addFina(this.getMain().getPizza()));
                break;
            case "gruesa":
                this.getMain().setPizza(FabricaPizzas.addGruesa(this.getMain().getPizza()));
                break;
            case "clasica":
                this.getMain().setPizza(FabricaPizzas.addClasica(this.getMain().getPizza()));
                break;
            case "tomate":
                this.getMain().setPizza(FabricaPizzas.addTomate(this.getMain().getPizza()));
                break;
            case "ranchera":
                this.getMain().setPizza(FabricaPizzas.addRanchera(this.getMain().getPizza()));
                break;
            case "barbacoa":
                this.getMain().setPizza(FabricaPizzas.addBarbacoa(this.getMain().getPizza()));
                break;
            case "emmental":
                this.getMain().setPizza(FabricaPizzas.addEmmental(this.getMain().getPizza()));
                break;
            case "gorgonzola":
                this.getMain().setPizza(FabricaPizzas.addGorgonzolla(this.getMain().getPizza()));
                break;
            case "parmesano":
                this.getMain().setPizza(FabricaPizzas.addParmesano(this.getMain().getPizza()));
                break;
            case "olivas":
                this.getMain().setPizza(FabricaPizzas.addOlivas(this.getMain().getPizza()));
                break;
            case "champis":
                this.getMain().setPizza(FabricaPizzas.addChampis(this.getMain().getPizza()));
                break;
            case "pepperoni":
                this.getMain().setPizza(FabricaPizzas.addPepperoni(this.getMain().getPizza()));
                break;
        }
    }

    public int getVidasPlayer() {
        return this.getMain().getVidasPlayer();
    }

    public void setVidasPlayer(int vidas) {
        this.getMain().setVidasPlayer(vidas);
    }

    public double getPuntuacion() {
        return this.getMain().getPuntuacion();
    }

    public double getPrecio() {
        return this.getMain().getPrecioPizza();
    }

    public void reiniciarPizza() {
        this.getMain().reiniciarPizza();
    }

    public void reiniciarPizzaMonster() {
        this.getMain().reiniciarPizzaMonster();
    }

    public boolean esPizzaSinIngredientes() {
        return (this.getMain().getPizza().getAnterior() == null);
    }

    public int compararPizzas() {
        return this.getMain().compararPizzas();
    }

    public MainLogica getMain() {
        return main;
    }

    public void setMain(MainLogica main) {
        this.main = main;
    }

    public JuegoControlador getControladorJuego() {
        return controladorJuego;
    }

    public void setControladorJuego(JuegoControlador controlador) {
        this.controladorJuego = controlador;
    }


    public String entregaPizza() {
        return this.nivel.entregaPizza();
    }

    public void seleccionarPizza() {
        this.nivel.seleccionarPizza();
    }

    public void contadorTiempo() {
        this.nivel.contadorTiempo();
    }

    public void setNivel(Nivel n) {
        this.nivel = n;
    }

    public Nivel getNivel() {
        return this.nivel;
    }

    public void incrementaVida() {
        if (main.getVidasPlayer() < 5) {
            main.ganaVida();
        }
    }


    public void decrementaVida() {
        if (main.getVidasPlayer() > 0) {
            main.pierdeVida();
        }
    }

    public void reiniciarJuego() {
        main.setVidasPlayer(5);
        main.setPuntuacion(0);
    }
}
