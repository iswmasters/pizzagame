package logica;


import logica.fabrica.FabricaPizzas;

public class MainLogica {
    private Monster monster;
    private PizzaInterface pizza;
    //private double temporizador;
    private int vidasPlayer;
    private double puntuacion;

    public MainLogica() {
        //this.setTemporizador(0);
        this.setVidasPlayer(5);
        this.setPuntuacion(0);
    }


    public Monster getMonster() {
        return monster;
    }

    public void setMonster(Monster monster) {
        this.monster = monster;
    }

    public PizzaInterface getPizza() {
        return pizza;
    }

    public void setPizza(PizzaInterface pizza) {
        this.pizza = pizza;
    }

    public int getVidasPlayer() {
        return vidasPlayer;
    }

    public void setVidasPlayer(int vidasPlayer) {
        this.vidasPlayer = vidasPlayer;
    }

    public void ganaVida() {
        this.vidasPlayer += 1;
    }

    public void pierdeVida() {
        this.vidasPlayer -= 1;
    }

//	public double getTemporizador() {
//		return temporizador;
//	}
//
//	public void setTemporizador(double temporizador) {
//		this.temporizador = temporizador;
//	}

    public double getPuntuacion() {
        return puntuacion;
    }

    public void setPuntuacion(double puntuacion) {
        this.puntuacion = puntuacion;
    }

    public void sumaPuntuacion() {
        this.puntuacion += this.pizza.calculaCoste();
    }

    public double getPrecioPizza() {
        return this.pizza.calculaCoste();
    }


    public int compararPizzas() {
        int res = 0;
        int cont = 0;
        PizzaInterface pizzaDecorada = this.getPizza();
        PizzaInterface pizzaMonster = this.getMonster().getPizza();
        while (pizzaDecorada.getAnterior() != null && pizzaMonster.getAnterior() != null) {
            cont++;
            if (pizzaDecorada.getDescripcion().equals(pizzaMonster.getDescripcion())) {
                res = cont;
            }
            pizzaDecorada = pizzaDecorada.getAnterior();
            pizzaMonster = pizzaMonster.getAnterior();
        }
        return res;
    }

    public PizzaInterface getPizzaBase() {
        PizzaInterface pizzaDecorada = this.getPizza();
        while (pizzaDecorada.getAnterior() != null) {
            pizzaDecorada = pizzaDecorada.getAnterior();
        }
        return pizzaDecorada;
    }

    public String respuestaMonster(int resp) {
        return this.getMonster().getRespuestas()[resp];

    }

    public void reiniciarPizza() {
        this.setPizza(this.getPizzaBase());
    }

    //Reinicia la pizza del Monster para volver a jugar, OJO QUE HAY QUE REINICIAR LA PIZZA ANTES!!
    public void reiniciarPizzaMonster() {
        this.setMonster(FabricaPizzas.crearMonster(FabricaPizzas.crearPizzaRand(this.getPizza())));
    }


}
