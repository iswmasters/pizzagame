package logica;

public interface PizzaInterface {
    public double calculaCoste();

    public String getDescripcion();

    public PizzaInterface getAnterior();

    public void setAnterior(PizzaInterface anterior);
}
