package logica.estrategia;

/**
 * Created by David on 13/05/2016.
 */
public interface Nivel {
    public void contadorTiempo();

    public String entregaPizza();

    public void setFachada();

    public void seleccionarPizza();
}
