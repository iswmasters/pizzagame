package logica.estrategia;

import logica.Fachada;
import logica.MainLogica;
import logica.fabrica.FabricaPizzas;
import presentacion.JuegoControlador;
import utils.CuentaAtras;

/**
 * Created by David on 13/05/2016.
 */
public class NivelMedio implements Nivel {
    Fachada fachada;
    private MainLogica main;
    private CuentaAtras contador;

    @Override
    public void seleccionarPizza() {

        this.setFachada();
        main = fachada.getMain();

        main.setPizza(FabricaPizzas.crearMedia());
        main.setMonster(FabricaPizzas.crearMonster(FabricaPizzas.crearPizzaRand(main.getPizza())));
    }


    @Override
    public void contadorTiempo() {

        contador = new CuentaAtras(24);

        this.setFachada();
        JuegoControlador controladorJuego = fachada.getControladorJuego();

        contador.setControladorJuego(controladorJuego);


    }

    @Override
    public String entregaPizza() {

        this.setFachada();
        main = fachada.getMain();


        int resp = main.compararPizzas();
        switch (resp) {
            case 0:
                main.sumaPuntuacion();
                fachada.incrementaVida(); //x1 vidas

                contador.reset();

                break;
            default:
                fachada.decrementaVida(); //x1 vidas
                if (main.getVidasPlayer() < 1) {
                    return "pierdePartida";
                }
        }
        return main.respuestaMonster(resp);
    }

    @Override
    public void setFachada() {
        fachada = Fachada.getFachada();
    }
}
