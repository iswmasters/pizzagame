package logica.decorador;

import logica.PizzaInterface;

public class MasaClasicaDecorator extends PizzaDecorator {

    public MasaClasicaDecorator(PizzaInterface pizzadecorada) {
        super(pizzadecorada);
    }

    @Override
    public String getDescripcion() {
        return "Masa clasica";
    }

    @Override
    public double calculaCoste() {
        return 0.50 + pizzaDecorada.calculaCoste();
    }

}
