package logica.decorador;

import logica.PizzaInterface;

public class SalsaRancheraDecorator extends PizzaDecorator {

    public SalsaRancheraDecorator(PizzaInterface pizzadecorada) {
        super(pizzadecorada);
    }

    @Override
    public String getDescripcion() {
        return "Salsa ranchera";
    }

    @Override
    public double calculaCoste() {
        return 1.00 + pizzaDecorada.calculaCoste();
    }

}
