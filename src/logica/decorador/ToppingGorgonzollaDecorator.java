package logica.decorador;

import logica.PizzaInterface;

public class ToppingGorgonzollaDecorator extends PizzaDecorator {

    public ToppingGorgonzollaDecorator(PizzaInterface pizzadecorada) {
        super(pizzadecorada);
    }

    @Override
    public String getDescripcion() {
        return "Topping gorgonzolla";
    }

    @Override
    public double calculaCoste() {
        return 1.00 + pizzaDecorada.calculaCoste();
    }

}
