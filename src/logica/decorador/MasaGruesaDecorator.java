package logica.decorador;

import logica.PizzaInterface;

public class MasaGruesaDecorator extends PizzaDecorator {

    public MasaGruesaDecorator(PizzaInterface pizzadecorada) {
        super(pizzadecorada);
    }

    @Override
    public String getDescripcion() {
        return "Masa gruesa";
    }

    @Override
    public double calculaCoste() {
        return 1.50 + pizzaDecorada.calculaCoste();
    }

}
