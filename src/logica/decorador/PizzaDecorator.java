package logica.decorador;

import logica.PizzaInterface;

public abstract class PizzaDecorator implements PizzaInterface {
    protected PizzaInterface pizzaDecorada;
    private PizzaInterface anterior;

    public PizzaDecorator(PizzaInterface pizzadecorada) {
        this.pizzaDecorada = pizzadecorada;
        this.setAnterior(pizzadecorada);
    }

    public String getDescripcion() {
        return pizzaDecorada.getDescripcion();
    }

    public double calculaCoste() {
        return pizzaDecorada.calculaCoste();
    }

    public PizzaInterface getAnterior() {
        return anterior;
    }

    public void setAnterior(PizzaInterface anterior) {
        this.anterior = anterior;
    }

}
