package logica.decorador;

import logica.PizzaInterface;

public class ExtraOlivasDecorator extends PizzaDecorator {

    public ExtraOlivasDecorator(PizzaInterface pizzadecorada) {
        super(pizzadecorada);
    }

    @Override
    public String getDescripcion() {
        return "Extra olivas";
    }

    @Override
    public double calculaCoste() {
        return 0.50 + pizzaDecorada.calculaCoste();
    }

}
