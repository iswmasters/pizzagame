package logica.decorador;

import logica.PizzaInterface;

public class SalsaBarbacoaDecorator extends PizzaDecorator {

    public SalsaBarbacoaDecorator(PizzaInterface pizzadecorada) {
        super(pizzadecorada);
    }

    @Override
    public String getDescripcion() {
        return "Salsa barbacoa";
    }

    @Override
    public double calculaCoste() {
        return 1.50 + pizzaDecorada.calculaCoste();
    }

}
