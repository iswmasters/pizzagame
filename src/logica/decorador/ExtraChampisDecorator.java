package logica.decorador;

import logica.PizzaInterface;

public class ExtraChampisDecorator extends PizzaDecorator {

    public ExtraChampisDecorator(PizzaInterface pizzadecorada) {
        super(pizzadecorada);
    }

    @Override
    public String getDescripcion() {
        return "Extra olivas";
    }

    @Override
    public double calculaCoste() {
        return 1.00 + pizzaDecorada.calculaCoste();
    }

}
