package logica.decorador;

import logica.PizzaInterface;

public class SalsaTomateDecorator extends PizzaDecorator {

    public SalsaTomateDecorator(PizzaInterface pizzadecorada) {
        super(pizzadecorada);
    }

    @Override
    public String getDescripcion() {
        return "Salsa tomate";
    }

    @Override
    public double calculaCoste() {
        return 0.50 + pizzaDecorada.calculaCoste();
    }

}
