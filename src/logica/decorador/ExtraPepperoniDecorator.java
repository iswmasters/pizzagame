package logica.decorador;

import logica.PizzaInterface;

public class ExtraPepperoniDecorator extends PizzaDecorator {

    public ExtraPepperoniDecorator(PizzaInterface pizzadecorada) {
        super(pizzadecorada);
    }

    @Override
    public String getDescripcion() {
        return "Extra pepperoni";
    }

    @Override
    public double calculaCoste() {
        return 1.50 + pizzaDecorada.calculaCoste();
    }

}
