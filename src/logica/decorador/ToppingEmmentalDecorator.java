package logica.decorador;

import logica.PizzaInterface;

public class ToppingEmmentalDecorator extends PizzaDecorator {

    public ToppingEmmentalDecorator(PizzaInterface pizzadecorada) {
        super(pizzadecorada);
    }

    @Override
    public String getDescripcion() {
        return "Topping emmental";
    }

    @Override
    public double calculaCoste() {
        return 0.50 + pizzaDecorada.calculaCoste();
    }

}
