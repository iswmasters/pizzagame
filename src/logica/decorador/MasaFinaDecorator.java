package logica.decorador;

import logica.PizzaInterface;

public class MasaFinaDecorator extends PizzaDecorator {

    public MasaFinaDecorator(PizzaInterface pizzadecorada) {
        super(pizzadecorada);
    }

    @Override
    public String getDescripcion() {
        return "Masa fina";
    }

    @Override
    public double calculaCoste() {
        return 1.00 + pizzaDecorada.calculaCoste();
    }

}
