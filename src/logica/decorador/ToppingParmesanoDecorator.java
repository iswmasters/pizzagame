package logica.decorador;

import logica.PizzaInterface;

public class ToppingParmesanoDecorator extends PizzaDecorator {

    public ToppingParmesanoDecorator(PizzaInterface pizzadecorada) {
        super(pizzadecorada);
    }

    @Override
    public String getDescripcion() {
        return "Topping parmesano";
    }

    @Override
    public double calculaCoste() {
        return 1.50 + pizzaDecorada.calculaCoste();
    }

}
