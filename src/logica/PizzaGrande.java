package logica;

public class PizzaGrande implements PizzaInterface {
    private PizzaInterface anterior;
    private String descripcion;
    private double precio;

    public PizzaGrande() {
        this.anterior = null;
        this.descripcion = "Pizza grande";
        this.precio = 15.0;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public double calculaCoste() {
        return precio;
    }

    public PizzaInterface getAnterior() {
        return anterior;
    }

    public void setAnterior(PizzaInterface anterior) {
        this.anterior = anterior;
    }
}
