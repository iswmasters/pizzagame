package logica;

import logica.estrategia.Nivel;
import logica.estrategia.NivelFacil;
import logica.fabrica.FabricaPizzas;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by David on 29/05/2016.
 */
public class FachadaTest {


    @Test
    public void testGetFachada() throws Exception {
        Fachada fachada1 = Fachada.getFachada();
        Fachada fachada2 = Fachada.getFachada();
        boolean comparacion = fachada1.equals(fachada2);
        assertTrue(comparacion);
    }

    @Test
    public void testGetVidasPlayer() throws Exception {
        Fachada fachada = Fachada.getFachada();
        MainLogica main = fachada.getMain();
        main.setVidasPlayer(2);
        int vidasFachada = fachada.getVidasPlayer();
        assertEquals(vidasFachada,2);

    }

    @Test
    public void testGetPuntuacion() throws Exception {
        Fachada fachada = Fachada.getFachada();
        MainLogica main = fachada.getMain();
        main.setPuntuacion(100);

        double puntuacionFachada = fachada.getPuntuacion();
        assertTrue(puntuacionFachada==100);
    }

    @Test
    public void testGetPrecio() throws Exception {
        Fachada fachada = Fachada.getFachada();
        PizzaPeque pizza = new PizzaPeque();
        fachada.getMain().setPizza(pizza);
        assertTrue(fachada.getPrecio() == pizza.calculaCoste());
    }

    @Test
    public void testReiniciarPizza() throws Exception {
        Fachada fachada = Fachada.getFachada();
        PizzaPeque pizza = new PizzaPeque();
        FabricaPizzas.addBarbacoa(pizza);
        fachada.getMain().setPizza(pizza);
        fachada.reiniciarPizza();
        assertTrue(fachada.getMain().getPizza().getAnterior() == null);
    }


    @Test
    public void testEsPizzaSinIngredientes() throws Exception {
        Fachada fachada = Fachada.getFachada();
        PizzaPeque pizza = new PizzaPeque();
        fachada.getMain().setPizza(pizza);
        assertTrue(fachada.esPizzaSinIngredientes());
    }

    @Test
    public void testCompararPizzas() throws Exception {
        Fachada fachada = Fachada.getFachada();
        PizzaPeque pizza = new PizzaPeque();
        PizzaPeque pizza2 = new PizzaPeque();
        FabricaPizzas.addBarbacoa(pizza);
        FabricaPizzas.addBarbacoa(pizza2);
        fachada.getMain().setPizza(pizza);
        fachada.getMain().setMonster(FabricaPizzas.crearMonster(pizza2));
        assertTrue(fachada.compararPizzas() == 0);

    }

    @Test
    public void testGetMain() throws Exception {
        Fachada fachada = Fachada.getFachada();
        MainLogica main = fachada.getMain();

        assertNotNull(main);
    }


    @Test
    public void testSetNivel() throws Exception {
        Fachada fachada = Fachada.getFachada();
        Nivel nivelFacil = new NivelFacil();
        fachada.setNivel(nivelFacil);

        Nivel nivelSeleccionado = fachada.getNivel();

        assertTrue( nivelSeleccionado instanceof NivelFacil);

    }

    @Test
    public void testIncrementaVida() throws Exception {
        Fachada fachada = Fachada.getFachada();
        fachada.setVidasPlayer(1);

        int vidasAntes = fachada.getVidasPlayer();
        fachada.incrementaVida();
        int vidasLuego = fachada.getVidasPlayer();

        assertTrue(vidasAntes<vidasLuego);
    }

    @Test
    public void testDecrementaVida() throws Exception {
        Fachada fachada = Fachada.getFachada();
        fachada.setVidasPlayer(1);

        int vidasAntes = fachada.getVidasPlayer();
        fachada.decrementaVida();
        int vidasLuego = fachada.getVidasPlayer();

        assertTrue(vidasAntes>vidasLuego);
    }

    @Test
    public void testReiniciarJuego() throws Exception {
        Fachada fachada = Fachada.getFachada();
        fachada.reiniciarJuego();
        assertTrue(fachada.getVidasPlayer() == 5 && fachada.getPuntuacion() == 0);
    }
}